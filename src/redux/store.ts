import { applyMiddleware, compose, createStore } from 'redux'
import createDebounce from 'redux-debounced'
import thunk from 'redux-thunk'

import { DrumPadReducer } from './DrumPadReducer'

const rootReducer = DrumPadReducer

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const middlewares = [createDebounce(), thunk]

const enhancer = composeEnhancers(applyMiddleware(...middlewares))

const store = createStore(rootReducer, undefined, enhancer)

export type Store = ReturnType<typeof rootReducer>
export { store }
