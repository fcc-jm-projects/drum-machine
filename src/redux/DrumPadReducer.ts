import { Dispatch } from 'react'

export type AudioFiles = {
   [key: string]: {
      [key: string]: string | number
      bank1: string
      bank2: string
      keyCode: number
   }
}

type InitialState = {
   audioFiles: AudioFiles
   currentBeat: string
   displayText: string
   powerState: boolean
   soundBank: number
   volumeLevel: string
}

const initialState: InitialState = {
   currentBeat: '',
   displayText: '',
   powerState: true,
   soundBank: 1,
   volumeLevel: '50',
   audioFiles: {
      Q: {
         bank1: `Bld_H1.mp3`,
         bank2: `Heater-1.mp3`,
         keyCode: 81
      },
      W: {
         bank1: `Brk_Snr.mp3`,
         bank2: `Heater-2.mp3`,
         keyCode: 87
      },
      E: {
         bank1: `Cev_H2.mp3`,
         bank2: `Heater-3.mp3`,
         keyCode: 69
      },
      A: {
         bank1: `Chord_1.mp3`,
         bank2: `Heater-4.mp3`,
         keyCode: 65
      },
      S: {
         bank1: `Chord_2.mp3`,
         bank2: `Heater-6.mp3`,
         keyCode: 83
      },
      D: {
         bank1: `Chord_3.mp3`,
         bank2: `Kick_n_Hat.mp3`,
         keyCode: 68
      },
      Z: {
         bank1: `Dry_Ohh.mp3`,
         bank2: `punchy_kick_1.mp3`,
         keyCode: 90
      },
      X: {
         bank1: `Dsc_Oh.mp3`,
         bank2: `RP4_KICK_1.mp3`,
         keyCode: 88
      },
      C: {
         bank1: `Give_us_a_light.mp3`,
         bank2: `side_stick_1.mp3`,
         keyCode: 67
      }
   }
}

/**
 * selectors
 */
const getCurrentBank = (state: InitialState) => `bank${state.soundBank}`
const getAudioFiles = (state: InitialState) => state.audioFiles
const getPowerState = (state: InitialState) => state.powerState
const getVolumeLevel = (state: InitialState) => state.volumeLevel
const getDisplayText = (state: InitialState) => state.displayText

/**
 * actions
 */
const POWER_BUTTON = 'POWER_BUTTON'
const TOGGLE_POWER_BUTTON = 'TOGGLE_POWER_BUTTON'
const BANK_BUTTON = 'BANK_BUTTON'
const TOGGLE_BANK_BUTTON = 'TOGGLE_BANK_BUTTON'
const CHANGE_CURRENT_BEAT = 'CHANGE_CURRENT_BEAT'
const UPDATE_VOLUME_LEVEL = 'UPDATE_VOLUME_LEVEL'
const RESET_DISPLAY_TO_BEAT = 'RESET_DISPLAY_TO_BEAT'

const toggleButtonState = (button: string) => ({
   type: `TOGGLE_${button}`
})

const changeCurrentBeat = (file: string) => {
   const splitfile = file.split('/')
   const baseName = splitfile[splitfile.length - 1]
      .split('.')[0]
      .replace(/_/g, ' ')
      .replace(/-/g, ' ')

   return {
      type: CHANGE_CURRENT_BEAT,
      payload: baseName
   }
}

const resetDisplayToCurrentBeat = (currentBeat: string) => ({
   type: RESET_DISPLAY_TO_BEAT,
   payload: currentBeat,
   meta: {
      debounce: {
         time: 500
      }
   }
})

const updateVolumeLevel = (level: string) => {
   return (dispatch: Dispatch<any>, getState: () => InitialState) => {
      const { currentBeat } = getState()

      dispatch({
         type: UPDATE_VOLUME_LEVEL,
         payload: level
      })

      dispatch(resetDisplayToCurrentBeat(currentBeat))
   }
}

/**
 * reducers
 */
const DrumPadReducer = (state = initialState, action: any) => {
   switch (action.type) {
      case TOGGLE_POWER_BUTTON:
         return { ...state, powerState: !state.powerState }
      case TOGGLE_BANK_BUTTON:
         return { ...state, soundBank: state.soundBank === 1 ? 2 : 1 }
      case CHANGE_CURRENT_BEAT:
         return { ...state, displayText: action.payload, currentBeat: action.payload }
      case RESET_DISPLAY_TO_BEAT:
         return { ...state, displayText: action.payload }
      case UPDATE_VOLUME_LEVEL:
         return {
            ...state,
            volumeLevel: action.payload,
            displayText: `Volume: ${action.payload}`
         }
      default:
         return state
   }
}

export {
   BANK_BUTTON,
   changeCurrentBeat,
   DrumPadReducer,
   getAudioFiles,
   getCurrentBank,
   getDisplayText,
   getPowerState,
   getVolumeLevel,
   POWER_BUTTON,
   TOGGLE_POWER_BUTTON,
   toggleButtonState,
   UPDATE_VOLUME_LEVEL,
   updateVolumeLevel
}
