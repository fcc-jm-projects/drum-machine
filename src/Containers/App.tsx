import React, { ChangeEvent } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { ToggleButton, TOGGLED_RIGHT } from '../Components/ToggleButton'
import { VolumeSlider } from '../Components/VolumeSlider'
import {
   BANK_BUTTON,
   getAudioFiles,
   getCurrentBank,
   getDisplayText,
   getVolumeLevel,
   POWER_BUTTON,
   toggleButtonState,
   updateVolumeLevel
} from '../redux/DrumPadReducer'
import { DrumPadBank } from './DrumPadBank'

const App: React.FC = () => {
   const dispatch = useDispatch()

   const audioFiles = useSelector(getAudioFiles)
   const audioBank = useSelector(getCurrentBank)
   const volumeLevel = useSelector(getVolumeLevel)
   const displayText = useSelector(getDisplayText)

   const handlePowerToggle = () => dispatch(toggleButtonState(POWER_BUTTON))

   const handleBankToggle = () => {
      dispatch(toggleButtonState(BANK_BUTTON))
   }

   const handleSliderChange = (e: ChangeEvent<HTMLInputElement>) => {
      dispatch(updateVolumeLevel(e.target.value))
   }

   return (
      <div className="container">
         <main className="machine-wrapper" id="drum-machine">
            <div className="logo-wrapper">FCC</div>
            <section className="drum-pad__bank--wrapper">
               <DrumPadBank audioBank={audioBank} audioFiles={audioFiles} />
            </section>
            <section className="controls__wrapper">
               <ToggleButton
                  title="Power"
                  startPosition={TOGGLED_RIGHT}
                  handleOnClick={handlePowerToggle}
                  className="controls__toggle__wrapper"
               />
               <div className="controls__display" id="display">
                  {displayText}
               </div>
               <VolumeSlider
                  volumeLevel={volumeLevel}
                  handleSliderChange={handleSliderChange}
                  className="controls__volume"
               />
               <ToggleButton
                  title="Bank"
                  handleOnClick={handleBankToggle}
                  className="controls__toggle__wrapper"
               />
            </section>
         </main>
      </div>
   )
}

export { App }
