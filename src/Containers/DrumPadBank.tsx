import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { AudioFiles, changeCurrentBeat, getPowerState, getVolumeLevel } from '../redux/DrumPadReducer'

export type DrumPadBankProps = {
   audioFiles: AudioFiles
   audioBank: string
}

const DrumPadBank: React.FC<DrumPadBankProps> = ({ audioFiles, audioBank }) => {
   const dispatch = useDispatch()
   const audioFileRefs = useRef<{ [key: string]: HTMLAudioElement }>({})

   const isOn = useSelector(getPowerState)
   const volumeLevel = useSelector(getVolumeLevel)

   const allFiles = Object.keys(audioFiles)

   const playFile = (key: string) => () => {
      if (isOn) {
         const audioFile = audioFileRefs.current[key]

         // the volume attribute ranges from 0.0 to 1.0
         audioFile.volume = Number(volumeLevel) / 100

         audioFile.play()
         dispatch(changeCurrentBeat(audioFile.src))
      }
   }

   const setRef = (key: string) => (el: HTMLAudioElement | null) => {
      if (el) audioFileRefs.current[key] = el
   }

   const handleKeyDown = ({ keyCode, key }: { key: string; keyCode: number }) => {
      const letter = allFiles.filter((l) => audioFiles[l].keyCode === keyCode)[0]

      if (allFiles.includes(letter)) {
         playFile(key.toUpperCase())()
      }
   }

   useEffect(() => {
      document.addEventListener('keydown', handleKeyDown)

      return () => document.removeEventListener('keydown', handleKeyDown)
   })

   return (
      <>
         {allFiles.map((key) => {
            return (
               <figure
                  className="drum-pad__container drum-pad"
                  key={key}
                  id={key}
                  onClick={playFile(key)}
               >
                  {key}
                  <audio
                     src={require(`../audio_files/${audioFiles[key][audioBank]}`)}
                     ref={setRef(key)}
                     className="clip"
                     id={key}
                  >
                     Your browser does not support the
                     <code>audio</code> element.
                  </audio>
               </figure>
            )
         })}
      </>
   )
}

export { DrumPadBank }
