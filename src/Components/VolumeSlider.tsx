import React, { ChangeEvent } from 'react'

type VolumeSliderProps = {
   className: string
   volumeLevel: string
   handleSliderChange: (e: ChangeEvent<HTMLInputElement>) => void
}

const VolumeSlider: React.FC<VolumeSliderProps> = ({
   className,
   volumeLevel,
   handleSliderChange
}) => {
   return (
      <div className={className}>
         <input
            type="range"
            name="volume"
            min="0"
            max="100"
            value={volumeLevel}
            onChange={handleSliderChange}
         />
      </div>
   )
}

export { VolumeSlider }
