import React, { useState } from 'react'

const TOGGLED_LEFT = 'toggled-left'
const TOGGLED_RIGHT = 'toggled-right'

type ToggleButtonProps = {
   className: string
   handleOnClick: () => void
   startPosition?: string
   title: string
}

const ToggleButton: React.FC<ToggleButtonProps> = ({
   className,
   handleOnClick,
   startPosition = TOGGLED_LEFT,
   title
}) => {
   const [position, setPosition] = useState(startPosition)

   const handleLocalOnClick = () => {
      setPosition(position === TOGGLED_LEFT ? TOGGLED_RIGHT : TOGGLED_LEFT)

      handleOnClick()
   }

   return (
      <div className={className}>
         <span>{title}</span>
         <div
            className={`toggle__container toggle__container--${position}`}
            onClick={handleLocalOnClick}
         >
            <div className="toggle" />
         </div>
      </div>
   )
}

export { ToggleButton, TOGGLED_LEFT, TOGGLED_RIGHT }
